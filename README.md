##### 1. How to use
If you prefer the built-in `sqlite` over a dedicated `mysql`, update your `.env`:
```diff
+COMPOSE_FILE=compose.yml
-COMPOSE_FILE=compose.yml:compose.mysql.yml
```

Stand them up:
```bash
docker-compose up -d && docker-compose logs -f
```

##### 2. Play around
Run test:
```bash
cd app/ && yarn run test
```
or:
```bash
docker-compose run --rm app yarn run test
```

Build the image (we're using [`buildx`](https://docs.docker.com/buildx/working-with-buildx/)):
```bash
cd app/ && DOCKER_BUILDKIT=1 docker build -t hinorashi/getting-started .
```
or:
```bash
docker-compose build
```
